#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

readonly debugMode=${DEBUG:-}
readonly testString="Promtail CRI autoconfig $RANDOM"
readonly templateFilename="${TEMPLATE_DIRECTORY:-/etc/promtail-template}/promtail.yaml"
readonly outputFilename="${OUTPUT_DIRECTORY:-/etc/promtail}/promtail.yaml"
readonly podsDirectory="${PODS_DIRECTORY:-/var/log/pods}"
readonly podUid=${THIS_POD_UID:-}
readonly podName=${THIS_POD_NAME:-}
readonly podNamespace=${THIS_POD_NAMESPACE:-}
readonly podLogDirectory="${podsDirectory}/${podNamespace}_${podName}_${podUid}/cri-autoconfig"

die() {
	printf "%b\n" "$@" >&2
	exit 1
}

printDebugSummary() {
	[ -n "$debugMode" ] || return 0
	printf "Promtail configuration template: %s\n" "$templateFilename"
	printf "Promtail configuration output:   %s\n" "$outputFilename"
	printf "Pods directory:                  %s\n" "$podsDirectory"
	printf "Search log directory:            %s\n" "$podLogDirectory"
}

printDebug() {
	[ -n "$debugMode" ] || return 0
	printf "%b\n" "$@"
}

main() {
	printDebugSummary

	[ -r "$templateFilename" ] || die "Promtail configuration file not found in $templateFilename"
	[ -n "$podUid" ] || die "Pod uid is not provided"
	[ -n "$podName" ] || die "Pod name is not provided"
	[ -n "$podNamespace" ] || die "Pod namespace is not provided"

	echo "$testString"
	sleep 1

	logFound=$(find "$podLogDirectory" ! -type d -iname '*.log' -exec grep "$testString" '{}' \; -print)
	[ -n "$logFound" ] || die "Pod log not found"

	logFilename=$(echo "$logFound" | tail -1 | tr -d '\n')
	printDebug "Test string found in $logFilename"

	logString=$(echo "$logFound" | grep -v "$logFilename" | tr -d '\n')
	printDebug "Log string content: $logString"

	logFormat=""

	if [[ $logString =~ ^\{\"log\":.*\}$ ]]; then
		logFormat="docker"
	elif [[ $logString =~ ^[0-9]{4}-[0-9]{2}-[0-9]{2}T ]]; then
		logFormat="cri"
	else
		die "Unknown log format in string: $logString"
	fi

	printDebug "Log format is $logFormat"

	cp "$templateFilename" "$outputFilename"
	export LOGFORMAT="$logFormat"
	sed -i -e 's LOGFORMAT '${LOGFORMAT}' g' "$outputFilename"

	if [ -n "$debugMode" ]; then
		stat "$outputFilename"
		cat "$outputFilename"
	fi
}

main
